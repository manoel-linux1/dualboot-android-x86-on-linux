#!/bin/bash

clear

if [[ $EUID -eq 0 ]]; then
echo " ███████ ██████  ██████   ██████  ██████  ██ "
echo " ██      ██   ██ ██   ██ ██    ██ ██   ██ ██ "
echo " █████   ██████  ██████  ██    ██ ██████  ██ "
echo " ██      ██   ██ ██   ██ ██    ██ ██   ██    "
echo " ███████ ██   ██ ██   ██  ██████  ██   ██ ██ "                                                                                        
echo "#################################################################"
echo "(This script should not be executed as a superuser or sudo)"
echo "(Please run it without superuser privileges or sudo)"
echo "#################################################################"
exit 1
fi

clear

echo "#################################################################"
echo "(Deleting the existing Android folder and creating the Android folder...)"
echo "#################################################################"

sudo rm -rf /android-data-2

sudo rm -rf /android

sudo rm -rf /android-ram

sudo mkdir -p android-data-2

sudo mkdir -p android

sudo mkdir -p android-ram

echo "#################################################################"
echo "(Downloading Android for x86_64...)"
echo "#################################################################"

wget https://sourceforge.net/projects/blissos-x86/files/Official/BlissOS15/FOSS/Generic/Bliss-v15.9.1-x86_64-OFFICIAL-foss-20240602.iso/download

echo "#################################################################"

clear

echo "#################################################################"
echo "(Mounting the android.iso media at /mnt...)"
echo "#################################################################"

sudo mount download /mnt/

echo "#################################################################"

clear

echo "#################################################################"
echo "(Installing android...)"
echo "#################################################################"

sudo cp -r /mnt/* android-ram/

sudo cp -r /mnt/* android/

sudo cp -r /mnt/* android-data-2/

echo "#################################################################"

sudo mkdir -p android/data

sudo mkdir -p android-data-2/data

echo "#################################################################"

sleep 3

sudo mv android-ram /android-ram

sudo mv android /android

sudo mv android-data-2 /android-data-2

sleep 3

echo "#################################################################"
echo "(Unmounting the android.iso media mounted at /mnt...)"
echo "#################################################################"

sudo umount /mnt/

echo "#################################################################"

clear

echo "#################################################################"
echo "(Deleting the android.iso media...)"
echo "#################################################################"

rm -rf download

echo "#################################################################"
echo "(Updating the GRUB configuration...)"
echo "#################################################################"

sudo grub-mkconfig -o /boot/grub/grub.cfg

echo "#################################################################"

clear

cd $HOME

clear

echo "#################################################################"
echo " ██████   ██████  ███    ██ ███████ ██ "
echo " ██   ██ ██    ██ ████   ██ ██      ██ "
echo " ██   ██ ██    ██ ██ ██  ██ █████   ██ "
echo " ██   ██ ██    ██ ██  ██ ██ ██         "
echo " ██████   ██████  ██   ████ ███████ ██ "  
echo "#################################################################"