#!/bin/bash

clear

if [[ $EUID -eq 0 ]]; then
echo " ███████ ██████  ██████   ██████  ██████  ██ "
echo " ██      ██   ██ ██   ██ ██    ██ ██   ██ ██ "
echo " █████   ██████  ██████  ██    ██ ██████  ██ "
echo " ██      ██   ██ ██   ██ ██    ██ ██   ██    "
echo " ███████ ██   ██ ██   ██  ██████  ██   ██ ██ "                                                                                        
echo "#################################################################"
echo "(This script should not be executed as a superuser or sudo)"
echo "(Please run it without superuser privileges or sudo)"
echo "#################################################################"
exit 1
fi

clear

echo "#################################################################"

sudo rm -rf /etc/grub.d/40_custom

sudo rm -rf /etc/grub.d/41_custom

sudo rm -rf /etc/grub.d/42_custom

echo "#################################################################"

clear

echo "#################################################################"

sudo cp 40_custom /etc/grub.d/

sudo cp 41_custom /etc/grub.d/

sudo cp 42_custom /etc/grub.d/

echo "#################################################################"

clear

echo "#################################################################"

sudo chmod +x /etc/grub.d/40_custom

sudo chmod +x /etc/grub.d/41_custom

sudo chmod +x /etc/grub.d/42_custom

echo "#################################################################"
echo "(Updating the GRUB configuration...)"
echo "#################################################################"

sudo grub-mkconfig -o /boot/grub/grub.cfg

echo "#################################################################"

clear

cd $HOME

clear

echo "#################################################################"
echo " ██████   ██████  ███    ██ ███████ ██ "
echo " ██   ██ ██    ██ ████   ██ ██      ██ "
echo " ██   ██ ██    ██ ██ ██  ██ █████   ██ "
echo " ██   ██ ██    ██ ██  ██ ██ ██         "
echo " ██████   ██████  ██   ████ ███████ ██ "  
echo "#################################################################"